FROM java:8
VOLUME /tmp
ARG APP_PATH=/spring-login
ARG ENVIRONMENT
RUN mkdir -p spring-login
COPY ./target/bank-account.jar ${APP_PATH}/bank-account.jar
WORKDIR ${APP_PATH}
EXPOSE 8080
CMD java -jar bank-account.jar