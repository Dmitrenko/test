package com.brightgrove.login.controller;

import com.brightgrove.login.exception.AccountAlreadyExists;
import com.brightgrove.login.exception.CreateBankAccountException;
import com.brightgrove.login.model.User;
import com.brightgrove.login.service.UserService;
import com.brightgrove.login.exception.BankAccountValidationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.security.Principal;

import static com.brightgrove.login.model.Pages.*;
import static org.springframework.security.crypto.bcrypt.BCrypt.*;
import static org.springframework.security.crypto.bcrypt.BCrypt.hashpw;

@Controller
public class AccountController {

    @Autowired
    private UserService userService;


    @GetMapping("/account")
    public String account(Principal principal,Model model) {
        User user = userService.getUserByUsername(principal.getName());
        model.addAttribute("user", user);
        return ACCOUNT_PAGE;
    }

    @GetMapping("/")
    public String index() {
        return HOME_PAGE;
    }

    @GetMapping("/new-account")
    public String newAccountPage() {
        return CHOOSE_ACCOUNT_PAGE;
    }

    @GetMapping("/create-account")
    public String createAccountPage() {
        return CREATE_ACCOUNT_PAGE;
    }

    @PostMapping("/create")
    private String createBankAccount(@Valid @ModelAttribute User user, BindingResult result) {
        if (result.hasErrors() || !user.getPassword()
                                      .equals(user.getRePassword())) {
            throw new BankAccountValidationException("Bank account has invalid arguments", "validation-error");
        }

        if (userService.getUserByEmailAndMobilePhone(user) != null) {
            throw new AccountAlreadyExists("Account already exists with this email and mobile phone", "already-exists");
        }

        try {
            user.setPassword(hashpw(user.getPassword(), gensalt()));
            userService.createBankAccount(user);
        } catch (RuntimeException e) {
            throw new CreateBankAccountException("Unable to create new bank account, try again later", "unpredicted-error");
        }

        return "redirect:/?status=successful";
    }
}
