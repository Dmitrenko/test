package com.brightgrove.login.exception;

public class CreateBankAccountException extends GenericException {
    public CreateBankAccountException(final String message, final String errorMessage) {
        super(message, errorMessage);
    }
}
