package com.brightgrove.login.exception;

public class AccountAlreadyExists extends GenericException {
    public AccountAlreadyExists(String message, String errorMessage) {
        super(message, errorMessage);
    }
}
