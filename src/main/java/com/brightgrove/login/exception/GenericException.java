package com.brightgrove.login.exception;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class GenericException extends RuntimeException {
    private String errorStatus;

    public GenericException(String message, String errorStatus) {
        super(message);
        this.errorStatus = errorStatus;
    }
}
