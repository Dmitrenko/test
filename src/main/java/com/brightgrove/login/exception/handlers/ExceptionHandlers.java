package com.brightgrove.login.exception.handlers;

import com.brightgrove.login.exception.BankAccountValidationException;
import com.brightgrove.login.exception.CreateBankAccountException;
import com.brightgrove.login.exception.GenericException;
import lombok.extern.log4j.Log4j2;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
@Log4j2
public class ExceptionHandlers {

    @ExceptionHandler(value = {CreateBankAccountException.class, UsernameNotFoundException.class, BankAccountValidationException.class, GenericException.class})
    public String createBankAccountException(GenericException exception) {
        log.error("Exception " + exception.getMessage(), exception);
        return "redirect:/?status=" + exception.getErrorStatus();
    }

}
