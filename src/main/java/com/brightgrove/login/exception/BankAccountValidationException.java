package com.brightgrove.login.exception;

public class BankAccountValidationException extends GenericException {

    public BankAccountValidationException(String message, String errorMessage) {
        super(message, errorMessage);
    }

}
