package com.brightgrove.login;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.messaging.converter.MappingJackson2MessageConverter;
import org.springframework.messaging.simp.stomp.StompSessionHandler;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.socket.client.WebSocketClient;
import org.springframework.web.socket.client.standard.StandardWebSocketClient;
import org.springframework.web.socket.messaging.WebSocketStompClient;

import java.io.IOException;
import java.util.Scanner;

@SpringBootApplication
@EnableTransactionManagement
public class LoginApplication {

    public static void main(String[] args) throws IOException {
        SpringApplication.run(LoginApplication.class, args);
    }
}
