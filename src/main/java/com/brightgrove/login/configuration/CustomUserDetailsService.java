package com.brightgrove.login.configuration;

import com.brightgrove.login.dao.UserDAO;
import com.brightgrove.login.exception.UserNotFoundException;
import com.brightgrove.login.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.Optional;

@Configuration
public class CustomUserDetailsService implements UserDetailsService {

    @Autowired
    private UserDAO userDAO;

    @Override
    public UserDetails loadUserByUsername(final String email) throws UsernameNotFoundException {
        Optional<User> optionalUser = userDAO.findById(email);

        optionalUser.orElseThrow(() -> new UserNotFoundException("Username not found with email: " + email));
        User user = optionalUser.get();
        return org.springframework.security.core.userdetails.User.withUsername(user.getEmail()).password(user.getPassword()).authorities("USER").build();
    }
}
