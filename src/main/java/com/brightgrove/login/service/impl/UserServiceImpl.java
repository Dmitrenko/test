package com.brightgrove.login.service.impl;

import com.brightgrove.login.dao.UserDAO;
import com.brightgrove.login.model.User;
import com.brightgrove.login.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

    private final UserDAO userDAO;

    @Autowired
    public UserServiceImpl(final UserDAO userDAO) {
        this.userDAO = userDAO;
    }

    public void createBankAccount(final User user) {
        userDAO.save(user);
    }

    @Override
    public User getUserByUsername(final String name) {
        return userDAO.findById(name).get();
    }

    @Override
    public User getUserByEmailAndMobilePhone(User user) {
        return userDAO.findByEmailAndPhoneNumber(user.getEmail(),user.getPhoneNumber());
    }
}
