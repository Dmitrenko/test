package com.brightgrove.login.service;

import com.brightgrove.login.model.User;

public interface UserService {
    void createBankAccount(final User user);

    User getUserByUsername(String name);

    User getUserByEmailAndMobilePhone(User user);

}
