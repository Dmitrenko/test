package com.brightgrove.login.dao;

import com.brightgrove.login.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserDAO extends JpaRepository<User, String> {
    User findByEmailAndPhoneNumber(String email, String phoneNumber);
}