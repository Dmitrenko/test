package com.brightgrove.login.model;

public enum PhoneType {
    HOME, WORK, MOBILE
}
