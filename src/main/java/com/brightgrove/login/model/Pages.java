package com.brightgrove.login.model;

public interface Pages {
    String ACCOUNT_PAGE = "personalaccount";
    String HOME_PAGE = "index";
    String CHOOSE_ACCOUNT_PAGE = "openaccount";
    String CREATE_ACCOUNT_PAGE = "createaccount";
}
