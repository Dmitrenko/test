package com.brightgrove.login.model;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Entity
@Table(name = "USER")
@Data
public class User {

    @Id
    @Column(name = "EMAIL", unique = true, nullable = false)
    @NotEmpty(message = "This field is mandatory")
    private String email;

    @Column(name = "FIRST_NAME", nullable = false)
    @NotEmpty(message = "This field is mandatory")
    private String firstName;

    @Column(name = "LAST_NAME", nullable = false)
    @NotEmpty(message = "This field is mandatory")
    private String lastName;

    @Column(name = "MIDDLE_NAME")
    private String middleName;

    @Column(name = "FIRST_ADDRESS", nullable = false)
    @NotEmpty(message = "This field is mandatory")
    private String firstAddress;

    @Column(name = "LAST_ADDRESS")
    private String lastAddress;

    @Column(name = "CITY", nullable = false)
    @NotEmpty(message = "This field is mandatory")
    private String city;

    @Column(name = "STATE", nullable = false)
    @NotEmpty(message = "This field is mandatory")
    private String state;

    @Column(name = "ZIP_CODE", nullable = false)
    @NotEmpty(message = "This field is mandatory")
    private String zipCode;

    @Column(name = "PHONE_NUMBER", unique = true, nullable = false)
    @NotEmpty(message = "This field is mandatory")
    private String phoneNumber;

    @Column(name = "PHONE_TYPE")
    @Enumerated(value = EnumType.STRING)
    @NotNull(message = "This field is mandatory")
    private PhoneType phoneType;

    @Column(name = "PASSWORD")
    @NotEmpty(message = "This field is mandatory")
    private String password;

    @Transient
    private String rePassword;

    @Column(name = "DATE_OF_BIRTH")
    @NotEmpty(message = "This field is mandatory")
    @Pattern(regexp = "^(([01])\\d)/(([012])\\d)/((19|20)\\d{2})")
    private String dateOfBirth;
}
