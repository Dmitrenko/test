create table if not exists user (
  email             varchar(255) not null ,
  city              varchar(255) not null,
  country_residence varchar(255),
  date_of_birth     varchar(255),
  dual_citizenship  boolean,
  first_address     varchar(255) not null,
  first_name        varchar(255) not null,
  last_address      varchar(255),
  last_name         varchar(255) not null,
  middle_name       varchar(255),
  password          varchar(255),
  phone_number      varchar(255) not null UNIQUE,
  phone_type        varchar(255),
  state             varchar(255) not null,
  us_citizen        boolean,
  zip_code          varchar(255) not null,
  primary key (email)
)
